import React, {Component} from 'react'
import {connect} from 'react-redux'
import {loadSmth} from '../actions/index'
import Pano from "./Pano";

class Schema extends Component {
    constructor(props) {
        super(props);
        this.state = [0, 0];
        this.fileInput = React.createRef();
    }

    handleSubmit(event) {
        event.preventDefault();
        this.props.loadSmth(this.fileInput.current.files[0], "ADD PANO", this.state);
        this.setState([0, 0]);
    }

    saveCoord(e) {
        this.setState([e.nativeEvent.offsetX, e.nativeEvent.offsetY]);
    }

    render() {
        if ((typeof this.props.state) == "object") {
            return (
                <div>
                    <img src={URL.createObjectURL(this.props.state.schema)} onClick={this.saveCoord.bind(this)}
                         style={{width: '300px'}}/>
                    <form onSubmit={this.handleSubmit.bind(this)}>
                        <label>
                            add pano
                            <input type="file" ref={this.fileInput}/>
                        </label>
                        coodrs :[{this.state[0]},{this.state[1]}]
                        <input type="submit" value="Отправить"/>

                    </form>
                    {this.props.state.panos.map((pano, i) => <Pano pano={pano} i={i}/>)}
                </div>
            );
        } else {
            return (
                <div>{this.props.state}</div>
            )
        }
    }
}

export default connect(state => ({state: state}), {loadSmth})(Schema)

