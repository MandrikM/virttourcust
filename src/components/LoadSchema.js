import React from 'react'
import {connect} from 'react-redux'
import {loadSmth} from '../actions/index'

class LoadSchema extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.fileInput = React.createRef();
    }

    handleSubmit(event) {
        event.preventDefault();
        this.props.loadSmth(this.fileInput.current.files[0], "LOAD SCHEMA");
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit.bind(this)}>
                <label>
                    Имя:
                    <input type="file" ref={this.fileInput}/>
                </label>
                <input type="submit" value="Отправить"/>
            </form>
        );
    }
}

export default connect(null, {loadSmth})(LoadSchema);