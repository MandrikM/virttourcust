import {put, takeEvery} from 'redux-saga/effects'

//import {linkBase} from './Pages.json'

export function* fetchPageAsync(action) {
    const api = action.pagenum
    const text = yield fetch(/*linkBase*/ +api)
        .then(response => response.json())

    yield put({type: "PAGE_SELECTED", payload: text.info})
}

export default function* pageAsync() {
    yield takeEvery('PAGE_ASYNC', fetchPageAsync)
}