import React from "react";
import LoadSchema from "./LoadSchema";
import Schema from "./Schema";

function Main() {
    return (
        <div>
            <LoadSchema/>
            <Schema/>
        </div>
    );
}

export default Main;
