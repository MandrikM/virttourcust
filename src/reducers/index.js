export default function (state = "no tours", action) {
    switch (action.type) {
        case "LOAD SCHEMA":
            return {schema: action.file, panos: [], imges: [], audios: [], docs: []};
        case "ADD PANO":
            let oldStatePano = state;
            oldStatePano.panos = oldStatePano.panos || [];
            oldStatePano.panos.push({file: action.file, coodr: action.coord})
            return oldStatePano;
        case "ADD AUDIO":
            let oldStateAudio = state;
            oldStateAudio.audios = oldStateAudio.audios || [];
            oldStateAudio.audios.push({file: action.file, coodr: action.coord, pano: action.pano})
            return oldStateAudio;
        case "ADD DOC":
            let oldStateDoc = state;
            oldStateDoc.docs = oldStateDoc.docs || [];
            oldStateDoc.docs.push({file: action.file, coodr: action.coord, pano: action.pano})
            return oldStateDoc;
        case "ADD IMG":
            let oldStateImg = state;
            oldStateImg.imges = oldStateImg.imges || [];
            oldStateImg.panos.push({file: action.file, coodr: action.coord, pano: action.pano})
            return oldStateAudio;
        default:
            return state;
    }
}